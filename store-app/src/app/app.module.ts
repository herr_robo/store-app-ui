import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { DepartmentsModule } from './departments/departments.module';
import { VideoGamesModule } from './video-games/video-games.module';
import { MusicModule } from './music/music.module';
import { ProductsModule } from './products/products.module';
import { WelcomeComponent } from './welcome/welcome.component';
import { ProductListComponent } from './products/product-list.component';
import { DepartmentsComponent } from './departments/departments.component';
import { MoviesShowsComponent } from './movies-shows/movies-shows.component';

import { InMemoryDataService } from './shared/in-memory-data.';
import { ShoppingCartModule } from './shopping-cart/shopping-cart.module';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';


@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    MoviesShowsComponent,

  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    InMemoryWebApiModule.forRoot(InMemoryDataService),
    DepartmentsModule,
    VideoGamesModule,
    MusicModule,
    ProductsModule,
    ShoppingCartModule,
    RouterModule.forRoot([
        { path: 'welcome', component: WelcomeComponent },
        { path: 'departments', component: DepartmentsComponent },
        { path: 'departments/moviesshows', component: MoviesShowsComponent },
        { path: 'products', component: ProductListComponent },
        { path: 'cart', component: ShoppingCartComponent},
        { path: '', redirectTo: 'welcome', pathMatch: 'full' },
        { path: '**', redirectTo: 'welcome', pathMatch: 'full' }
    ]),
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
