import { InMemoryDbService } from 'angular-in-memory-web-api';

import { IDepartment } from '../departments/departments';
import { IVideoGame } from '../video-games/videogame';
import { IProduct } from '../products/product';

export class InMemoryDataService implements InMemoryDbService {
    createDb() {
        const departments: IDepartment[] = [
            {
                id: 1,
                departmentName: "Video Games",
                departmentDescription: "From consoles to pc games",
                imageUrl: ""
            },
            {
                id: 2,
                departmentName: "Movies & Shows",
                departmentDescription: "Mostly cartoons and anime",
                imageUrl: ""
            },
            {
                id: 3,
                departmentName: "Music",
                departmentDescription: "Our catalog contains the best music that you need. We might be a little bias, but our musical taste is objectively right.",
                imageUrl: ""
            }
        ];

        const videogames: IVideoGame[] = [
            {
                id: 1,
                name: "Escape From Tarkov",
                platform: "PC",
                developers: "Battlestate Games",
                publisher: "Battlestate Games",
                summary: "Escape from Tarkov is a hardcore and realistic online first-person action RPG/Simulator with MMO features and a story-driven walkthrough.",
                genres: [ "First Person Shooter" ],
                website: "https://www.escapefromtarkov.com",
                release_date_NA: "",
                release_date_EUR: "",
                release_date_JP: "",
                release_date_AUS: "",
                price: 34.99,
                esrb_rating: "",
                extended_information: "With each passing day the situation in the Norvinsk region grows more and more complicated. Incessant warfare in Tarkov has sparked massive panic; the local population has fled the city, but those who stayed are looking to improve their fortunes at the expense of others. Having accepted the new reality, savage Tarkov locals - \"Scavs\" flocked into well-armed gangs and started the redivision of the city. Nowadays, Tarkov is separated by unseen borders, controlled by different groups. Gain-greedy gunmen would go to any length to have their way, including the murder of civilians and direct confrontation with the two private military companies. \n The players will have to experience living in the skin of one of the mercenaries who survived the initial stage of the Tarkov conflict. After choosing one of the sides – USEC or BEAR – the player’s character starts to make his way out of the city. Tarkov is sealed off by UN and Russian military, supply chains are cut, communication with operational command is lost, and in these conditions everyone has to make his own choices of what to do and how to get out of the chaos-ridden metropolis."
              },
              {
                  id: 2,
                  name: "DayZ",
                  platform: "PC",
                  developers: "Bohemia Interactive",
                  publisher: "Bohemia Interactive",
                  summary: "DAYZ is a gritty, authentic, open-world survival horror hybrid-MMO game, in which players follow a single goal: to survive in the harsh post-apocalyptic landscape as long as they can. Players can live through powerful events and emotions arising from the ever-evolving emergent gameplay.",
                  genres: [ "Zombies", "Action", "Horror", "Survival", "Multiplayer", "Open World" ],
                  website: "https://dayz.com/",
                  release_date_NA: "July 9, 2012",
                  release_date_EUR: "July 9, 2012",
                  release_date_JP: "",
                  release_date_AUS: "",
                  price: 34.99,
                  esrb_rating: "M",
                  extended_information: "-Chernarus: 230 sq. km chunk of post soviet state, featuring deep forests, cities, villages, abandoned military bases, and more.. \n -Up to 40 players per server: Rich server / client architecture developed from scratch. \n -Persistent player profile: Key player data is persistent across all servers. \n -Inventory: Combine various clothing items, equipment and carry all of the items that you will need to survive the harsh new environment in the open and intuitive inventory system developed to answer the needs of crafting and character customization. \n -Crafting: In the apocalyptic world of DayZ, resources are sparse and every useful bit should be utilized. Items or equipment can be improved, turned into something else or fixed using the new crafting mechanisms. \n -Weapon customization: Use customizable firearms with authentic ballistics and weapon characteristics as well as melee weapons to protect yourself against all the dangers a survivor may face. \n -Customizable clothing: Customize your survivor with a multitude of new clothing items that can be scavenged from the remnants of civilization, which provide various degree of protection against the elements, infected, and other survivors."
                },
                {
                  id: 3,
                  name: "RimWorld",
                  platform: "PC",
                  developers: "Ludeon Studios",
                  publisher: "Ludeon Studios",
                  summary: "RimWorld is a sci-fi colony sim driven by an intelligent AI storyteller. Inspired by Dwarf Fortress, Firefly, and Dune.",
                  genres: [ "Simulation", "Base Building", "Survival" ],
                  website: "https://rimworldgame.com/",
                  release_date_NA: "October 17, 2018",
                  release_date_EUR: "",
                  release_date_JP: "",
                  release_date_AUS: "",
                  price: 34.99,
                  esrb_rating: "",
                  extended_information: "RimWorld is a story generator. It’s designed to co-author tragic, twisted, and triumphant stories about imprisoned pirates, desperate colonists, starvation and survival. It works by controlling the “random” events that the world throws at you. Every thunderstorm, pirate raid, and traveling salesman is a card dealt into your story by the AI Storyteller. There are several storytellers to choose from. Randy Random does crazy stuff, Cassandra Classic goes for rising tension, and Phoebe Chillax likes to relax. \n Your colonists are not professional settlers – they’re crash-landed survivors from a passenger liner destroyed in orbit. You can end up with a nobleman, an accountant, and a housewife. You’ll acquire more colonists by capturing them in combat and turning them to your side, buying them from slave traders, or taking in refugees. So your colony will always be a motley crew."
                },
                {
                  id: 4,
                  name: "7 Days to Die",
                  platform: "PC",
                  developers: "The Fun Pimps Entertainment LLC",
                  publisher: "The Fun Pimps Entertainment LLC",
                  summary: "7 Days to Die is an open-world game that is a unique combination of first person shooter, survival horror, tower defense, and role-playing games. Play the definitive zombie survival sandbox RPG that came first. Navezgane awaits!",
                  genres: [ "First Person Shooter", "Horror", "Action", "Adventure"],
                  website: "https://7daystodie.com/",
                  release_date_NA: "December 13, 2013",
                  release_date_EUR: "December 13, 2013",
                  release_date_JP: "",
                  release_date_AUS: "",
                  price: 24.99,
                  esrb_rating: "",
                  extended_information: "Building on survivalist and horror themes, players in 7 Days to Die can scavenge the abandoned cities of the buildable and destructible voxel world for supplies or explore the wilderness to gather raw materials to build their own tools, weapons, traps, fortifications and shelters. In coming updates these features will be expanded upon with even more depth and a wider variety of choices to survive the increasing dangers of the world. Play alone or with friends, run your own server or join others. \n 7 Days to Die development was launched following a successful Kickstarter campaign in August 2013, demonstrating the initial prototype. Joining us in early access now will help support the cause and ensure continued expansion of the development team, their efforts and the planning and addition of even more features and depth eagerly awaited by fans. \n Many of our fans have commented in these early stages that they feel we are building the survival\\zombie game they have always wanted. Our goal is to live up to this as much as we can and build a customizable, moddable game that allows users to fine tune their survival and zombie experience to create the experience players and their friends want."
                },
                {
                  id: 5,
                  name: "911 Operator",
                  platform: "PC",
                  developers: "Jutsu Games",
                  publisher: "Games Operators",
                  summary: "Game about the difficult work of people that manage emergency lines and services. Your task is to answer incoming calls and to react properly - give first aid instructions, advise, dispatch correct number of firemen / police / ambulances, or sometimes - just ignore the call. Play on ANY CITY in the world!",
                  genres: [ "Strategy", "Simulation", "Management" ],
                  website: "",
                  release_date_NA: "February 24, 2017",
                  release_date_EUR: "",
                  release_date_JP: "",
                  release_date_AUS: "",
                  price: 24.99,
                  esrb_rating: "T",
                  extended_information: "In 911 OPERATOR, you take on the role of an emergency dispatcher, who has to rapidly deal with the incoming reports. Your task is not just to pick up the calls, but also to react appropriately to the situation – sometimes giving first aid instructions is enough, at other times a police, fire department or paramedics’ intervention is a necessity. Keep in mind, that the person on the other side of the line might turn out to be a dying daughter’s father, an unpredictable terrorist, or just a prankster. Can you handle all of this?"
                },
                {
                  id: 6,
                  name: "Cook, Serve, Delicious!",
                  platform: "PC",
                  developers: "Vertigo Gaming Inc.",
                  publisher: "Vertigo Gaming Inc.",
                  summary: "One of the few hardcore restaurant sims in existence, Cook, Serve, Delicious! is a deceptively easy game to learn but incredibly challenging to master as you progress through your career from owning a terrible zero star cafe into a five star world famous restaurant.",
                  genres: [ "Simulation", "Indie", "Management", "Casual", "Typing" ],
                  website: "http://www.vertigogaming.net/CSD/",
                  release_date_NA: "October 8, 2013",
                  release_date_EUR: "",
                  release_date_JP: "",
                  release_date_AUS: "",
                  price: 9.99,
                  esrb_rating: "",
                  extended_information: "Cook, Serve, Delicious is a hardcore restaurant sim, one of the few in its genre that gives the players total control over where they want to take their restaurant. \n The game centers on an old, worn down restaurant in the SherriSoda Tower, which was once the heart of the building but closed down as business (and tenants) dropped to an all time low. But with the local economy rising back up and occupancy reaching its maximum, SherriSoda Tower has decided to bring back the old Cook, Serve, Delicious restaurant, and has given you complete control to bring it back to its old five star status of world class dining. \n To do that, you’ll start with a few thousand dollars and thirty foods to choose from to put on your menu, as well as a host of restaurant equipment and more. Nearly everything is unlocked from the start of the game for you to purchase; this is your restaurant, and you must figure out the best strategy to get that restaurant packed with satisfied customers. Can you prove your culinary skills?"
                },
                {
                  id: 7,
                  name: "Cook, Serve, Delicious! 2!!",
                  platform: "PC",
                  developers: "Vertigo Gaming Inc.",
                  publisher: "Vertigo Gaming Inc.",
                  summary: "The highest selling and most intense restaurant sim ever made is back! Deceptively easy to learn but incredibly difficult to master, Cook, Serve, Delicious! 2!! is your journey of being the best chef in the world with your small but humble restaurant on the 50th floor of the Teragon Supertower.",
                  genres: [ "Simulation", "Indie", "Management", "Casual", "Typing" ],
                  website: "http://www.cookservedelicious.com/main/",
                  release_date_NA: "September 13, 2017",
                  release_date_EUR: "September 13, 2017",
                  release_date_JP: "September 13, 2017",
                  release_date_AUS: "September 13, 2017",
                  price: 12.99,
                  esrb_rating: "E10+",
                  extended_information: "Cook, Serve, Delicious! 2!! is the massive sequel to the surprise best selling original, one of the few games that gives players complete control on how they want to build their restaurant. \n The game starts like any other morning at SherriSoda Tower as you take the elevator up to open Cook, Serve, Delicious!, a platinum star restaurant that was surging in popularity thanks to your amazing chef and management skills. Just then, a swarm of police surround SherriSoda Tower. It seems the SherriSoda head executives were secretly stealing funds from the company at the same time they were incurring a staggering amount of debt, draining the accounts of the tower and several of the businesses inside of it, including CSD. Just like that, the tower was closed and put up for federal auction, including everything inside of it. It was all over… the Cook, Serve, Delicious! restaurant was no more. \n Angry but determined to rebuild, you’ve scrounged up all of your personal life’s savings and bought commercial space inside the Teragon Supertower, the largest skyscraper in the city. It’s here that you will start a brand new Cook, Serve, Delicious! restaurant, build it back to its former glory, and rebuild your legacy as the best chef in the world."
                },
                {
                  id: 8,
                  name: "The Final Station",
                  platform: "PC",
                  developers: "Do My Best",
                  publisher: "tinyBuild",
                  summary: "Travel by train through a dying world. Look after your passengers, keep your train operational, and make sure you can always reach the next station. Make your way through swarms of infected at each station. Explore mysterious and abandoned stations looking for supplies and survivors.",
                  genres: [ "Pixel Graphic", "Survival", "Indie", "Post-apocalyptic" ],
                  website: "http://www.cookservedelicious.com/main/",
                  release_date_NA: "August 30, 2016",
                  release_date_EUR: "",
                  release_date_JP: "",
                  release_date_AUS: "",
                  price: 14.99,
                  esrb_rating: "",
                  extended_information: "The world is over. But it’s not quite over for you… at least, not yet. And now that you’ve got thousands of tonnes of locomotive at your disposal, ready to take you as far down the tracks as you have fuel to feed it, you’ve got the definite advantage over the infected hordes. The real question is whether or not you'll help the survivors get to their destinations.... or let them die and loot their bodies. Sometimes people can be more trouble than they're worth. \n This is The Final Station."
                },
                {
                  id: 9,
                  name: "Starbound",
                  platform: "PC",
                  developers: "Chucklefish",
                  publisher: "Chucklefish",
                  summary: "You’ve fled your home, only to find yourself lost in space with a damaged ship. Your only option is to beam down to the planet below, repair your ship and set off to explore the universe...",
                  genres: [ "Action", "Adventure", "Sandbox", "Survival" ],
                  website: "https://playstarbound.com/",
                  release_date_NA: "July 22, 2016",
                  release_date_EUR: "",
                  release_date_JP: "",
                  release_date_AUS: "",
                  price: 14.99,
                  esrb_rating: "E10+",
                  extended_information: "In Starbound, you take on the role of a character who’s just fled from their home planet, only to crash-land on another. From there you’ll embark on a quest to survive, discover, explore and fight your way across an infinite universe. \n You’ll encounter procedurally generated creatures and weapons, discover populated villages and abandoned temples. Explore planets dotted with dungeons, eyeball trees and treasure. Make use of over a hundred materials and over one thousand in-game objects to build a sprawling modern metropolis or a sleepy secluded cabin in the woods, and do all of it alone or with friends! \n Starbound lets you live out your own story of space exploration, discovery and adventure. Settle down and farm the land, hop from planet to planet claiming resources, or make regular visits to populated settlements, taking on jobs and earning a living. NPCs are scattered about the worlds, offering quests and challenges for those looking for a little extra excitement in their lives. \n Traverse the stars with the starmap to find endless procedurally generated planets and creatures. Discover new crops, creatures and weapons, capture monsters to keep as companions and clear dungeons of their loot. \n Complete quest lines that let you delve into the greater galactic mysteries! Who created the Glitch? What is Big Ape’s secret? Why are the Hylotl so uptight? From abandoned bunkers to creepy temples, Starbound’s richly detailed universe offers a host of places to investigate and tales to unfold. Follow the story or create your own, experience the game how YOU want to."
                }
        ];

        const products: IProduct[] = [
            {
                id: 1,
                productName: "risus semper porta",
                productPrice: "$81.08",
                productDescription: "maecenas tristique est et tempus semper est quam pharetra magna",
                productOnSale: false,
                productStarRating: 4.44,
                departmentId: 4,
                imageUrl: ""
              }, {
                id: 2,
                productName: "tristique fusce",
                productPrice: "$46.85",
                productDescription: "proin risus praesent lectus vestibulum quam sapien varius ut blandit non interdum in ante vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia",
                productOnSale: false,
                productStarRating: 1.84,
                departmentId: 4,
                imageUrl: ""
              }, {
                id: 3,
                productName: "rhoncus aliquam lacus",
                productPrice: "$12.51",
                productDescription: "sit amet cursus id turpis integer aliquet massa id lobortis convallis tortor risus dapibus augue vel accumsan tellus nisi eu orci mauris lacinia sapien quis libero nullam sit amet turpis",
                productOnSale: false,
                productStarRating: 4.79,
                departmentId: 2,
                imageUrl: ""
              }, {
                id: 4,
                productName: "potenti",
                productPrice: "$12.26",
                productDescription: "tortor duis mattis egestas metus aenean fermentum donec ut mauris eget massa tempor convallis nulla neque libero convallis eget eleifend luctus ultricies eu nibh quisque id justo sit amet",
                productOnSale: false,
                productStarRating: 1.42,
                departmentId: 1,
                imageUrl: ""
              }, {
                id: 5,
                productName: "iaculis",
                productPrice: "$76.50",
                productDescription: "pede justo eu massa donec dapibus duis at velit eu est congue elementum in hac habitasse",
                productOnSale: false,
                productStarRating: 2.08,
                departmentId: 4,
                imageUrl: ""
              }, {
                id: 6,
                productName: "elementum",
                productPrice: "$45.75",
                productDescription: "at velit eu est congue elementum in hac habitasse platea dictumst morbi",
                productOnSale: true,
                productStarRating: 3.63,
                departmentId: 3,
                imageUrl: ""
              }, {
                id: 7,
                productName: "ut volutpat",
                productPrice: "$70.51",
                productDescription: "ipsum aliquam non mauris morbi non lectus aliquam sit amet diam in magna bibendum imperdiet nullam orci pede venenatis non sodales",
                productOnSale: false,
                productStarRating: 3.81,
                departmentId: 2,
                imageUrl: ""
              }, {
                id: 8,
                productName: "eget congue eget",
                productPrice: "$69.21",
                productDescription: "lorem integer tincidunt ante vel ipsum praesent blandit lacinia erat vestibulum sed magna at nunc commodo placerat praesent blandit nam nulla integer pede justo lacinia eget tincidunt eget tempus vel",
                productOnSale: true,
                productStarRating: 3.2,
                departmentId: 4,
                imageUrl: ""
              }, {
                id: 9,
                productName: "tempus vel",
                productPrice: "$52.73",
                productDescription: "ante vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae duis faucibus accumsan odio curabitur convallis duis consequat dui nec nisi",
                productOnSale: true,
                productStarRating: 1.71,
                departmentId: 1,
                imageUrl: ""
              }, {
                id: 10,
                productName: "enim blandit mi",
                productPrice: "$33.36",
                productDescription: "interdum mauris ullamcorper purus sit amet nulla quisque arcu libero rutrum ac lobortis",
                productOnSale: false,
                productStarRating: 1.29,
                departmentId: 4,
                imageUrl: ""
              }, {
                id: 11,
                productName: "et",
                productPrice: "$35.93",
                productDescription: "venenatis non sodales sed tincidunt eu felis fusce posuere felis sed lacus morbi",
                productOnSale: false,
                productStarRating: 4.2,
                departmentId: 2,
                imageUrl: ""
              }, {
                id: 12,
                productName: "nullam varius",
                productPrice: "$90.93",
                productDescription: "rhoncus mauris enim leo rhoncus sed vestibulum sit amet cursus id turpis integer aliquet massa id lobortis convallis tortor risus dapibus augue vel accumsan tellus nisi",
                productOnSale: true,
                productStarRating: 4.99,
                departmentId: 3,
                imageUrl: ""
              }, {
                id: 13,
                productName: "sed justo pellentesque",
                productPrice: "$91.74",
                productDescription: "sit amet erat nulla tempus vivamus in felis eu sapien cursus vestibulum",
                productOnSale: false,
                productStarRating: 1.76,
                departmentId: 3,
                imageUrl: ""
              }, {
                id: 14,
                productName: "consequat",
                productPrice: "$99.67",
                productDescription: "rutrum nulla tellus in sagittis dui vel nisl duis ac nibh fusce lacus purus aliquet at",
                productOnSale: true,
                productStarRating: 3.68,
                departmentId: 3,
                imageUrl: ""
              }, {
                id: 15,
                productName: "ultrices posuere",
                productPrice: "$87.21",
                productDescription: "pellentesque volutpat dui maecenas tristique est et tempus semper est quam pharetra magna ac consequat metus sapien ut nunc vestibulum ante ipsum primis in",
                productOnSale: true,
                productStarRating: 3.51,
                departmentId: 3,
                imageUrl: ""
              }, {
                id: 16,
                productName: "eu sapien",
                productPrice: "$73.32",
                productDescription: "lectus vestibulum quam sapien varius ut blandit non interdum in ante vestibulum ante ipsum primis in",
                productOnSale: true,
                productStarRating: 1.92,
                departmentId: 1,
                imageUrl: ""
              }, {
                id: 17,
                productName: "mallur",
                productPrice: "$81.16",
                productDescription: "molestie hendrerit at vulputate vitae nisl aenean lectus pellentesque eget nunc donec quis orci eget",
                productOnSale: true,
                productStarRating: 1.21,
                departmentId: 4,
                imageUrl: ""
              }, {
                id: 18,
                productName: "eget",
                productPrice: "$94.28",
                productDescription: "donec posuere metus vitae ipsum aliquam non mauris morbi non lectus aliquam sit amet diam in magna bibendum imperdiet nullam",
                productOnSale: false,
                productStarRating: 3.19,
                departmentId: 4,
                imageUrl: ""
              }, {
                id: 19,
                productName: "enim",
                productPrice: "$66.70",
                productDescription: "posuere felis sed lacus morbi sem mauris laoreet ut rhoncus aliquet pulvinar sed nisl nunc rhoncus dui vel sem sed sagittis nam",
                productOnSale: false,
                productStarRating: 4.99,
                departmentId: 2,
                imageUrl: ""
              }, {
                id: 20,
                productName: "morbi",
                productPrice: "$8.32",
                productDescription: "vel lectus in quam fringilla rhoncus mauris enim leo rhoncus sed vestibulum sit",
                productOnSale: false,
                productStarRating: 1.99,
                departmentId: 2,
                imageUrl: ""
              }, {
                id: 21,
                productName: "in",
                productPrice: "$73.54",
                productDescription: "ipsum integer a nibh in quis justo maecenas rhoncus aliquam lacus morbi quis tortor id nulla ultrices aliquet maecenas leo odio",
                productOnSale: true,
                productStarRating: 3.2,
                departmentId: 4,
                imageUrl: ""
              }, {
                id: 22,
                productName: "dui proin",
                productPrice: "$2.95",
                productDescription: "commodo placerat praesent blandit nam nulla integer pede justo lacinia eget tincidunt eget tempus vel pede morbi porttitor lorem id ligula suspendisse ornare consequat lectus in",
                productOnSale: false,
                productStarRating: 1.57,
                departmentId: 2,
                imageUrl: ""
              }, {
                id: 23,
                productName: "nulla justo aliquam",
                productPrice: "$26.77",
                productDescription: "sapien dignissim vestibulum vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae nulla dapibus dolor vel",
                productOnSale: true,
                productStarRating: 4.39,
                departmentId: 3,
                imageUrl: ""
              }, {
                id: 24,
                productName: "in porttitor",
                productPrice: "$52.82",
                productDescription: "phasellus id sapien in sapien iaculis congue vivamus metus arcu adipiscing molestie hendrerit at vulputate",
                productOnSale: false,
                productStarRating: 1.89,
                departmentId: 1,
                imageUrl: ""
              }, {
                id: 25,
                productName: "ante vel",
                productPrice: "$29.00",
                productDescription: "amet lobortis sapien sapien non mi integer ac neque duis bibendum morbi non quam nec dui",
                productOnSale: false,
                productStarRating: 4.49,
                departmentId: 4,
                imageUrl: ""
              }
        ];

        return { departments, videogames, products };
    }

}
