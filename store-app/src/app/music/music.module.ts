import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MusicComponent } from './music.component';
import { MusicSearchComponent } from './music-search.component';
import { MusicRoutingModule } from './music-routing.module';

@NgModule({
    declarations: [
        MusicComponent,
        MusicSearchComponent
    ],
    imports: [
        CommonModule,
        MusicRoutingModule
    ]
})

export class MusicModule { }
