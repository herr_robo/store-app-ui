import { Component } from "@angular/core";

@Component({
    templateUrl: './welcome.component.html'
})

export class WelcomeComponent {
    welcomeMessage: string = 'Welcome to the Peepo Shopping Application!';
    summary: string = 'We offer only the finest offerings recommended by Peepo. From video games, to music, to anime, whatever Peepo suggests, we sell it.'
}
