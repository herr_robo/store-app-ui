
export interface IVideoGame {
    id: number;
    name: string;
    platform: string;
    developers: string;
    publisher: string;
    summary: string;
    genres: string[];
    website: string;
    release_date_NA: string;
    release_date_EUR?: string;
    release_date_JP?: string;
    release_date_AUS?: string;
    price: number;
    esrb_rating?: string;
    extended_information: string;
}
