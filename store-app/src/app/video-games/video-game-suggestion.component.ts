import { Component } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { VideoGameSuggestionModalComponent } from './video-game-suggestion-modal.component';

@Component({
    selector: 'video-game-suggestion',
    templateUrl: './video-game-suggestion.component.html',
    styleUrls: ['./video-game-suggestion.component.css']
})

export class VideoGameSuggestionComponent {
    suggestMessage: string = 'Don\'t see a game here that you would like to purchase? Click below to suggest a game we should add to the store!';

    constructor(private modalService: NgbModal) {}

    openSuggestionModal() {
        console.log('modal open');
        const modalRef = this.modalService.open(VideoGameSuggestionModalComponent)
    }


}
