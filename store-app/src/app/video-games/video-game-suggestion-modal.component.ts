import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormControl, FormArray, FormBuilder, Validators } from '@angular/forms';

@Component({
    selector: 'video-game-suggestion-modal',
    templateUrl: './video-game-suggestion-modal.component.html',
    styleUrls: ['./video-game-suggestion-modal.component.css']
})

export class VideoGameSuggestionModalComponent {

    form: FormGroup;

    publisherControl: FormControl;
    platformControl: FormControl;
    genreControl: FormControl;
    estimatedPriceControl: FormControl;

    platforms: Array<any> = [
        {
            name: 'PC',
            value: 'pc'
        },
        {
            name: 'Console',
            value: 'console'
        },
    ];

    constructor(public activeModal: NgbActiveModal, private fb: FormBuilder) {

        this.form = this.fb.group({
            titleControl: ['', Validators.required ],
            publisherControl: ['', Validators.required],
            platformControl: ['', Validators.required],
            genreControl: ['', Validators.required],
            estimatedPriceControl: ['', Validators.required]
        })
    }

}
