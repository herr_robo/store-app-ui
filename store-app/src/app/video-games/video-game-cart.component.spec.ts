import { ComponentFixture, TestBed } from "@angular/core/testing";
import { VideoGameCartComponent } from './video-game-cart.component';

describe(('VideoGameCartComponent'), () => {

    describe('removeFromCart', () => {
        let cartList;

        let fixture: ComponentFixture<VideoGameCartComponent>;
        let component: VideoGameCartComponent;

        beforeEach(() => {
            cartList = [
                {id: 1, name: "Escape From Tarkov", price: 34.99},
                {id: 2, name: "DayZ", price: 34.99},
                {id: 3, name: "RimWorld", price: 34.99},
                {id: 4, name: "7 Days to Die", price: 24.99}
            ];

            TestBed.configureTestingModule({
                declarations: [VideoGameCartComponent]
            });

            fixture = TestBed.createComponent(VideoGameCartComponent);
            component = fixture.componentInstance;
        })

        it('should have default values on initialization for total and cartList', () => {
            fixture.detectChanges();
            expect(component.cartList.length).toBe(0);
            expect(component.total).toBe(0);
        })

        it('should remove the selected item from cartList', () => {
            fixture.detectChanges();
            component.cartList = cartList;

            expect(component.cartList.length).toBe(4);
            component.removeFromCart(2)
            expect(component.cartList.length).toBe(3);
        })

        it('should not remove item from cartList if index provided < 0', () => {
            fixture.detectChanges();
            component.cartList = cartList;

            expect(component.cartList.length).toBe(4);
            component.removeFromCart(-1)
            expect(component.cartList.length).toBe(4);
        })
    })
})
