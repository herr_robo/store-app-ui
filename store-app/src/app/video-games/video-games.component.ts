import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    templateUrl: './video-games.component.html'
})

export class VideoGamesComponent {
    constructor(private router: Router) { }

    backToDepartments() {
        this.router.navigate(['/departments']);
    }
}
