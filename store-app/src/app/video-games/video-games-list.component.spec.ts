import { VideoGamesListComponent } from "./video-games-list.component";
import { ComponentFixture, TestBed, tick } from '@angular/core/testing';
import { VideoGameService } from './video-game.service';
import { of } from 'rxjs';
import { By } from '@angular/platform-browser';

describe('VideoGamesListComponent', () => {
    let component: VideoGamesListComponent;
    let fixture: ComponentFixture<VideoGamesListComponent>
    let GAMES;
    let mockVideoGameService;

    beforeEach(() => {

        GAMES = [
            { id:1, name:"Escape From Tarkov", platform:"PC", developers:"Battlestate Games",
              publisher:"Battlestate Games", summary:"", genres:["First Person Shooter"],
              website:"https://www.escapefromtarkov.com", release_date_NA:"", release_date_EUR:"",
              release_date_JP:"", release_date_AUS:"", price:34.99, esrb_rating:"", extended_information:""
            },
            { id:2, name:"DayZ", platform:"PC", developers:"Bohemia Interactive", publisher:"Bohemia Interactive",
                summary:"", genres:[ "Zombies", "Action", "Horror", "Survival", "Multiplayer", "Open World" ],
                website:"https://dayz.com/", release_date_NA:"", release_date_EUR:"", release_date_JP:"",
                release_date_AUS:"", price:34.99, esrb_rating:"", extended_information:""
            },
            { id:3, name:"RimWorld", platform:"PC", developers:"Ludeon Studios", publisher:"Ludeon Studios",
                summary:"", genres:[ "Simulation", "Base Building", "Survival" ],
                website:"https://rimworldgame.com/", release_date_NA:"", release_date_EUR:"", release_date_JP:"",
                release_date_AUS:"", price:34.99, esrb_rating:"", extended_information:""
            }
        ]

        mockVideoGameService = jasmine.createSpyObj(['getVideoGameList']);

        component = new VideoGamesListComponent(mockVideoGameService);

        TestBed.configureTestingModule({
            declarations: [VideoGamesListComponent],
            providers: [
                { provide: VideoGameService, useValue: mockVideoGameService }
            ]
        })

        fixture = TestBed.createComponent(VideoGamesListComponent);
    })

    it('should set videogames(list) correctly from the service', () => {
        mockVideoGameService.getVideoGameList.and.returnValue(of(GAMES));
        fixture.detectChanges();

        expect(fixture.componentInstance.videogames.length).toBe(3);
    })

    it('should create a list-group-item for each game', () => {
        mockVideoGameService.getVideoGameList.and.returnValue(of(GAMES));
        fixture.detectChanges();

        expect(fixture.debugElement.queryAll(By.css('.list-group-item')).length).toBe(3);
    })


    it('should set the selectedGame to the correct game', () => {
        mockVideoGameService.getVideoGameList.and.returnValue(of(GAMES));
        fixture.detectChanges();

        let buttons = fixture.debugElement.queryAll(By.css('.list-group-item button'));

        buttons[2].triggerEventHandler('click', null);

     //   tick();
        fixture.detectChanges();

        expect(fixture.componentInstance.selectedGame.name).toBe('RimWorld');
    })

})
