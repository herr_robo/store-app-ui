import { Component, Input, OnInit } from '@angular/core';
import { ICartItem } from './cart-item';

@Component({
    selector: 'video-game-cart',
    templateUrl: './video-game-cart.component.html',
    styleUrls: ['./video-game-cart.component.css']
})

export class VideoGameCartComponent implements OnInit {

    @Input() item : ICartItem;
    cartList: ICartItem[];
    total: number;

    constructor() {}

    ngOnInit() {
        this.total = 0;
        this.cartList = [];
    }

    ngOnChanges(changes) {
        if (this.item && this.cartList && this.cartList.length > 0) {
            this.pushGame(this.item);
            this.total = this.calculateTotal(this.cartList);
        }
        else if (this.item && this.cartList && this.cartList.length === 0) {
            this.cartList = [this.item];
            this.total = this.calculateTotal(this.cartList);
        }
    }

    removeFromCart(index) {
        if (index > -1) {
            this.cartList.splice(index, 1);
            this.total = this.calculateTotal(this.cartList);
        }
    }

    private pushGame(index) {
        this.cartList.push({
            id: this.item.id,
            name: this.item.name,
            price: this.item.price
        })
    }

    private calculateTotal(list: ICartItem[]) : number {
        let sum = 0;
        if (list.length > 0) {
            list.forEach(item => {
                sum += item.price;
            })
        }
        return sum;
    }

}
