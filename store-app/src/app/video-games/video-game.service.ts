import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { IVideoGame } from './videogame';

@Injectable({
    providedIn: 'root'
})

export class VideoGameService {

    private gamesDataUrl = 'api/videogames';

    constructor(private http: HttpClient) { }

    getVideoGameList() : Observable<IVideoGame[]> {
        return this.http.get<IVideoGame[]>(this.gamesDataUrl).pipe(
            tap(data => console.log(JSON.stringify(data)))
        );
    }

}
