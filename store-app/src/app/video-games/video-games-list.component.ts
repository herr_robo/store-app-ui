import { Component, OnInit } from '@angular/core';
import { VideoGameService } from './video-game.service';
import { IVideoGame } from './videogame';
import { ICartItem } from './cart-item';


@Component({
    selector: 'video-games-list',
    templateUrl: './video-games-list.component.html'
})

export class VideoGamesListComponent implements OnInit {

    videogames : IVideoGame[] = [];
    selectedGame : IVideoGame;
    addedGame: ICartItem;

    constructor(private videogameService: VideoGameService) {}

    ngOnInit() : void {
        this.getGames();
    }

    setSelectedGame(game) {
        this.selectedGame = game;
    }

    getGames() : void {
        this.videogameService.getVideoGameList().subscribe({
            next: videogames => {
                this.videogames = videogames;
            }
        })
    }

    addGameToCart(gameId) {
        let gameIndex = this.videogames.findIndex(game => game.id === gameId);

        if (gameIndex > -1) {
            this.addedGame = {
                id: this.videogames[gameIndex].id,
                name: this.videogames[gameIndex].name,
                price: this.videogames[gameIndex].price
            }
        }
        else {
            //game not found
            console.log('game not found');
        }

    }

}
