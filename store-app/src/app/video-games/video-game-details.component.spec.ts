import { VideoGameDetailsComponent } from "./video-game-details.component";
import { Component, DebugElement } from '@angular/core';
import { ComponentFixture, async, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

describe('VideoGameDetailsComponent', () => {

    describe('ngOnChanges related', () => {
        let component: VideoGameDetailsComponent;
        let fixture: ComponentFixture<TestHostComponent>;
        let testHost;
        let SELECTED = {
            id: 1,
            name: "Escape From Tarkov",
            platform: "PC",
            developers: "Battlestate Games",
            publisher: "Battlestate Games",
            esrb_rating: "",
            extended_information: "",
            genres: ["First Person Shooter"],
            price: 34.99,
            release_date_AUS: "",
            release_date_EUR: "",
            release_date_JP: "",
            release_date_NA: "",
            summary: "",
            website: "",
        }

        beforeEach(() => {
            TestBed.configureTestingModule({
                declarations: [TestHostComponent, VideoGameDetailsComponent]
            })

            fixture = TestBed.createComponent(TestHostComponent);
            testHost = fixture.componentInstance;

        })

        it('should have correct title displayed', () => {
            testHost.selectedGame = SELECTED;
            fixture.detectChanges();
            console.log(fixture.nativeElement.querySelector('.card-title'));

            expect(fixture.nativeElement.querySelector('.card-title').innerText).toBe('Escape From Tarkov');
        })

        @Component({
            template: '<video-game-details [selectedGameInput]="selectedGame"></video-game-details>'
        })

        class TestHostComponent {
            selectedGame: any;
        }
    })

    describe('addGame', () => {
        let fixture: ComponentFixture<VideoGameDetailsComponent>
        let component: VideoGameDetailsComponent;
        let button: DebugElement;
        let SELECTED = {
            id: 1,
            name: "Escape From Tarkov",
            platform: "PC",
            developers: "Battlestate Games",
            publisher: "Battlestate Games",
            esrb_rating: "",
            extended_information: "",
            genres: ["First Person Shooter"],
            price: 34.99,
            release_date_AUS: "",
            release_date_EUR: "",
            release_date_JP: "",
            release_date_NA: "",
            summary: "",
            website: "",
        }

        beforeEach(() => {
            TestBed.configureTestingModule({
                declarations: [VideoGameDetailsComponent]
            });

            fixture = TestBed.createComponent(VideoGameDetailsComponent);
            component = fixture.componentInstance;
            component.currentGame = SELECTED;
            fixture.detectChanges();
            button = fixture.debugElement.query(By.css('.btn-primary'));
            console.log(button);
        })

        it('should emit when the button is clicked', () => {
            spyOn(component.sendAddedGame, 'emit');
            button.nativeElement.click();
            expect(component.sendAddedGame.emit).toHaveBeenCalledWith(1);
        });
    })


})
