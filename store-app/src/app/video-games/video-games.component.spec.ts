
import { Router } from '@angular/router';
import { VideoGamesComponent } from './video-games.component';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

describe('VideoGamesComponent', () => {

    let fixture: ComponentFixture<VideoGamesComponent>;
    let routerSpy = { navigate: jasmine.createSpy('navigate') };

    beforeEach(() => {
        TestBed.configureTestingModule({
            declarations: [VideoGamesComponent],
            providers: [
                { provide: Router, useValue: routerSpy }
            ]
        })

        fixture = TestBed.createComponent(VideoGamesComponent);
    })

    it('should call router.navigate with /departments', () => {
        let button = fixture.debugElement.queryAll(By.css('.back-button'));
        button[0].triggerEventHandler('click', null);

        expect(routerSpy.navigate).toHaveBeenCalledWith(['/departments']);
    })
})
