import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VideoGamesComponent } from './video-games.component';

const videoGamesRoutes: Routes = [
    { path: 'departments/videogames', component: VideoGamesComponent },
];

  @NgModule({
    imports: [
      RouterModule.forChild(videoGamesRoutes)
    ],
    exports: [
      RouterModule
    ]
  })
  export class VideoGamesRoutingModule { }
