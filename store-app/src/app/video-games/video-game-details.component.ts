import { Component, Input, Output, EventEmitter } from '@angular/core';
import { IVideoGame } from './videogame';

@Component({
    selector: 'video-game-details',
    templateUrl: './video-game-details.component.html'
})

export class VideoGameDetailsComponent {
    @Input() selectedGameInput : IVideoGame;
    @Output() sendAddedGame = new EventEmitter<number>();
    currentGame: IVideoGame;

    constructor() {}

    ngOnChanges() {
        this.currentGame = this.selectedGameInput;
    }

    addGame(gameId) {
        this.sendAddedGame.emit(gameId);
    }
}
