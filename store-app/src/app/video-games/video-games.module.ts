import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { VideoGamesComponent } from './video-games.component';
import { VideoGamesListComponent } from './video-games-list.component';
import { VideoGameDetailsComponent } from './video-game-details.component';
import { VideoGamesRoutingModule } from './video-games-routing.module';
import { VideoGameCartComponent } from './video-game-cart.component';
import { VideoGameSuggestionComponent } from './video-game-suggestion.component';
import { VideoGameSuggestionModalComponent } from './video-game-suggestion-modal.component';

@NgModule({
    declarations: [
        VideoGamesComponent,
        VideoGamesListComponent,
        VideoGameDetailsComponent,
        VideoGameCartComponent,
        VideoGameSuggestionComponent,
        VideoGameSuggestionModalComponent
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        VideoGamesRoutingModule,
    ]
  })

  export class VideoGamesModule { }
