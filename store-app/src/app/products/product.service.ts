import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { IProduct } from './product';


@Injectable({
    providedIn: 'root'
})

export class ProductService {

    private productsUrl = 'api/products';

    constructor(private http: HttpClient) { }

    getProducts() : Observable<IProduct[]> {
        return this.http.get<IProduct[]>(this.productsUrl).pipe(
            tap(data => console.log(JSON.stringify(data)))
        );
    }

}
