import { Component, OnInit } from "@angular/core";
import { IProduct } from './product';
import { ProductService } from './product.service';


@Component({
    templateUrl: './product-list.component.html',
    styleUrls: ['./product-list.component.css']
})

export class ProductListComponent implements OnInit {
    message: string = 'Product List Component';
    productList: IProduct[] =[];
    errorMessage: string = 'An error has occurred';

    constructor(private productService: ProductService) { }

    ngOnInit() : void {
        this.productService.getProducts().subscribe({
            next: products => {
                this.productList = products;
            }
        });
    }


}
