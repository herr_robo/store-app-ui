
export interface IProduct {
    id: number;
    productName: string;
    productPrice: string;
    productDescription: string;
    productOnSale: boolean;
    productStarRating: number;
    departmentId: number;
    imageUrl: string;
}
