import { of } from "rxjs";
import { DepartmentsComponent } from './departments.component';
import { TestBed, ComponentFixture } from '@angular/core/testing';
import { DepartmentsService } from './departments.service';
import { By } from '@angular/platform-browser';

describe('DepartmentsComponent', () => {
    let component: DepartmentsComponent;
    let fixture: ComponentFixture<DepartmentsComponent>;
    let DEPARTMENTS;
    let mockDepartmentsService;
    beforeEach(() => {

        DEPARTMENTS = [
            { id: 1, departmentName: 'Video Games', departmentDescription: '', imageUrl: '' },
            { id: 2, departmentName: 'Movies & Shows', departmentDescription: '', imageUrl: '' },
            { id: 3, departmentName: 'Music', departmentDescription: '', imageUrl: '' }
        ]
        mockDepartmentsService = jasmine.createSpyObj(['getDepartments']);

        component = new DepartmentsComponent(mockDepartmentsService);

        TestBed.configureTestingModule({
            declarations: [DepartmentsComponent],
            providers: [
                { provide: DepartmentsService, useValue: mockDepartmentsService }
            ]
        })

        fixture = TestBed.createComponent(DepartmentsComponent);
    })

    it('should set departmentsList correctly from the service', () => {
        mockDepartmentsService.getDepartments.and.returnValue(of(DEPARTMENTS));
        fixture.detectChanges();

        expect(fixture.componentInstance.departmentsList.length).toBe(3);
    })

    it ('should create card for each department', () => {
        mockDepartmentsService.getDepartments.and.returnValue(of(DEPARTMENTS));
        fixture.detectChanges();

        expect(fixture.debugElement.queryAll(By.css('.card')).length).toBe(3);
    })
})
