import { Component, OnInit } from '@angular/core';
import { DepartmentsService } from './departments.service';
import { IDepartment } from './departments';

@Component({
    templateUrl: './departments.component.html',
    styleUrls: ['./departments.component.css']
})

export class DepartmentsComponent implements OnInit {
    departmentsList: IDepartment[] = [];

    constructor(private departmentsService: DepartmentsService) { }

    ngOnInit() : void {
        this.getDepartments();
    }

    getDepartments(): void {
        this.departmentsService.getDepartments().subscribe({
            next: departments => {
                this.departmentsList = departments;
            }
        });
    }

}
