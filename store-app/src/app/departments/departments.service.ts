import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { IDepartment } from './departments';

@Injectable({
    providedIn: 'root'
})

export class DepartmentsService {

    private departmentsUrl = 'api/departments';

    constructor(private http: HttpClient) { }

    getDepartments() : Observable<IDepartment[]> {
        return this.http.get<IDepartment[]>(this.departmentsUrl).pipe(
            tap(data => console.log(JSON.stringify(data)))
        );
    }
}
